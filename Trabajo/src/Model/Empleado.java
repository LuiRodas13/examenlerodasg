
package Model;

public class Empleado {
    String FirstName;
    String SecondName;
    String LastName;
    String LastName2;
    String Birthday;
    int Age;
    int sueldo;

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }
    String DateContract;
    int Experience;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String SecondName) {
        this.SecondName = SecondName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getLastName2() {
        return LastName2;
    }

    public void setLastName2(String LastName2) {
        this.LastName2 = LastName2;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String Birthday) {
        this.Birthday = Birthday;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }


    public String getDateContract() {
        return DateContract;
    }

    public void setDateContract(String DateContract) {
        this.DateContract = DateContract;
    }

    public int getExperience() {
        return Experience;
    }

    public void setExperience(int Experience) {
        this.Experience = Experience;
    }
    
    
}
