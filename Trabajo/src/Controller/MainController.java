
package Controller;

import Model.Empleado;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import views.EmpleadoFrame;
import views.EmpleadonewFrame;


public class MainController implements ActionListener {
      EmpleadoFrame mainframe;
      
    @Override
       public void actionPerformed(ActionEvent e) {

        switch(e.getActionCommand()){
            case "ViewsEmpleado":
                Empleado[] readEmpleadoList;
                break;
            
           
       }
    }
      public void showChildnewEmpleado(){
        EmpleadonewFrame e = new EmpleadonewFrame();
        mainframe.showChild(e, false);
    
      }
      
      private Empleado readEmpleado(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Empleado)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(mainframe, e.getMessage(),mainframe.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private Empleado[] readEmpleadoList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Empleado[] empleados;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
             empleados = (Empleado[]) s.readObject();
        }
        return empleados;
    }
}
